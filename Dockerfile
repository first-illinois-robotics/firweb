FROM alpine:3.18

RUN apk add --no-cache --update python3 py3-pip poetry yarn mariadb-connector-c-dev gcc musl-dev terraform openssh jq py3-mysqlclient rsync && \
    apk add --no-cache --update --virtual=build python3-dev libffi-dev openssl-dev cargo make g++ && \
    pip3 install --no-cache-dir --prefer-binary ansible[azure] azure-cli azure-keyvault-secrets msrest azure-identity && \
    apk del build