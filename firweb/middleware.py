from django.http import HttpRequest, HttpResponsePermanentRedirect


def www_middleware(get_response):
    def middleware(request: HttpRequest):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        host = request.get_host()
        if len(host.split(".")) == 2:
            return HttpResponsePermanentRedirect(f"https://www.{host}{request.get_full_path()}")
        response = get_response(request)
        return response

    return middleware

