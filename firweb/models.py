from cms.models.pluginmodel import CMSPlugin

from django.db import models

DONATION_PROGRAMS = [
    ("FIR", "<i>FIRST</i> Illinois Robotics"),
    ("FLLC", "<i>FIRST</i> LEGO® League Challenge"),
    ("FLLE", "<i>FIRST</i> LEGO® League Explore"),
    ("FTC", "<i>FIRST</i> Tech Challenge"),
    ("FRC", "<i>FIRST</i> Robotics Competition"),
    ("MWR", "FRC - Midwest Regional"),
    ("CIR", "FRC - Central Illinois Regional")
]


class Donation(CMSPlugin):
    default_program = models.TextField(max_length=50, choices=DONATION_PROGRAMS, blank=True)
