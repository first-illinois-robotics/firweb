import os

import environ  # type: ignore
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .common import BASE_DIR


env = environ.Env()
env_file = os.path.join(BASE_DIR, ".env")

env.read_env(env_file)

sentry_sdk.init(
    dsn=env.str("SENTRY_DSN"),
    environment=env.str("SENTRY_ENV"),
    integrations=[DjangoIntegration()],
    send_default_pii=True,
    traces_sample_rate=1.0,
    profiles_sample_rate=1.0,
)

DEBUG = False

SECRET_KEY = env("SECRET_KEY")

DATABASES = {
    "default": env.db(),
}

STORAGES = {
    "default": {"BACKEND": "firweb.backends.MediaAzureStorage"},
    "staticfiles": {"BACKEND": "firweb.backends.StaticAzureStorage"},
}
AZURE_ACCOUNT_NAME = env.str("AZURE_STORAGE_ACCOUNT_NAME")
AZURE_ACCOUNT_KEY = env.str("AZURE_STORAGE_ACCOUNT_KEY")
AZURE_CUSTOM_DOMAIN = env.str("AZURE_STORAGE_CUSTOM_DOMAIN")

STATIC_URL = f"https://{AZURE_CUSTOM_DOMAIN}/static/"
MEDIA_URL = f"https://{AZURE_CUSTOM_DOMAIN}/media/"

TEXT_CKEDITOR_BASE_PATH = f"{STATIC_URL}djangocms_text_ckeditor/ckeditor/"

FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': "firweb.backends.MediaAzureStorage",
        },
        'thumbnails': {
            'ENGINE': "firweb.backends.MediaAzureStorage",
        },
    },
}

DJANGOCMS_GOOGLEMAP_API_KEY = env.str("GMAPS_API")

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379",
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

ALLOWED_HOSTS = [".firstillinoisrobotics.org"]

# Various security settings
# see https://www.django-cms.org/en/blog/2022/02/22/security-enhancements-for-django-cms/

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True
SECURE_REDIRECT_EXEMPT = [r'^_events/', r'^_ah/']

SECURE_HSTS_SECONDS = 31536000  # 1 Year
SECURE_HSTS_INCLUDE_SUBDOMAINS = False
SECURE_HSTS_PRELOAD = True

CMS_TOOLBAR_ANONYMOUS_ON = False

SECURE_REFERRER_POLICY = "strict-origin-when-cross-origin"

# redefining entire template object so we can enable cached templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(BASE_DIR, "firweb", "templates"),
        ],
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.template.context_processors.media",
                "django.template.context_processors.csrf",
                "django.template.context_processors.tz",
                "sekizai.context_processors.sekizai",
                "django.template.context_processors.static",
                "cms.context_processors.cms_settings",
                "firweb.context_processors.program",
            ],
            "loaders": [
                ("django.template.loaders.cached.Loader", [
                    "django.template.loaders.filesystem.Loader",
                    "django.template.loaders.app_directories.Loader"
                ])
            ],
        },
    },
]


