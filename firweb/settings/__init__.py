from .common import *

if os.environ.get("FIRWEB_PROD", None):
    # both prod and staging use the same config, the secrets will just import from different projects
    from .prod import *
else:
    try:
        import livereload
        INSTALLED_APPS.insert(
            INSTALLED_APPS.index("django.contrib.staticfiles"), "livereload"
        )
    except ImportError:
        pass
    from .dev import *
