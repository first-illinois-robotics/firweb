# Generated by Django 4.2.7 on 2023-11-27 22:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("firweb", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="donation",
            name="default_program",
            field=models.TextField(
                blank=True,
                choices=[
                    ("FIR", "<i>FIRST</i> Illinois Robotics"),
                    ("FLLC", "<i>FIRST</i> LEGO® League Challenge"),
                    ("FLLE", "<i>FIRST</i> LEGO® League Explore"),
                    ("FTC", "<i>FIRST</i> Tech Challenge"),
                    ("FRC", "<i>FIRST</i> Robotics Competition"),
                    ("MWR", "FRC - Midwest Regional"),
                    ("CIR", "FRC - Central Illinois Regional"),
                ],
                max_length=50,
            ),
        ),
    ]
