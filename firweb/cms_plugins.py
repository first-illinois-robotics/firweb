from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

from .models import Donation

@plugin_pool.register_plugin
class EmptyPlugin(CMSPluginBase):
    model = CMSPlugin
    render_template = "empty.html"
    cache = True
    allow_children = True
    name = "Hide Child Plugins"

@plugin_pool.register_plugin
class ProgramPlugin(CMSPluginBase):
    model = CMSPlugin
    render_template = "donations/value.html"
    cache = False
    name = "Selected Program"
    text_enabled = True
    module = "Invoice"

    lookup_value = "item_name"

    def render(self, context, instance, placeholder):
        context["value"] = context['request'].GET.get(self.lookup_value, [""])
        print(context['request'].GET, self.lookup_value, context["value"])
        return context


@plugin_pool.register_plugin
class NotePlugin(ProgramPlugin):
    lookup_value = "custom"
    name = "Donation Note"


@plugin_pool.register_plugin
class AmountPlugin(ProgramPlugin):
    lookup_value = "amount"
    name = "Donation Amount"

@plugin_pool.register_plugin
class DonationPlugin(CMSPluginBase):
    model = Donation
    render_template = "donations/donation.html"
    cache = True
    name = "Donation Widget"