from storages.backends.azure_storage import AzureStorage
from django.conf import settings


class StaticAzureStorage(AzureStorage):
    azure_container = "static"


class MediaAzureStorage(AzureStorage):
    azure_container = "media"
