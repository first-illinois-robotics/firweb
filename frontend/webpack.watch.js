const { merge } = require('webpack-merge');
const dev = require('./webpack.dev.js');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const path = require("path");

module.exports = merge(dev, {
    output: {
        publicPath: "http://localhost:9001/"
    },
    devServer: {
        port: 9001,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
        },
        devMiddleware: {
            writeToDisk: true,
        },
        hot: false,
        allowedHosts: "all"
    },
    plugins: [
        new HtmlWebpackHarddiskPlugin()
    ],
});
