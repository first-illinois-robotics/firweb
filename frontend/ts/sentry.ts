import * as Sentry from "@sentry/browser";

Sentry.init({
    dsn: "https://972cf791200c822e6b2ac3b15ec1fb91@o4505733467865088.ingest.sentry.io/4506063259238400",

    // release: process.env.CI_COMMIT_SHORT_SHA,
    integrations: [new Sentry.BrowserTracing(), new Sentry.Replay()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,

    // Capture Replay for 10% of all sessions,
    // plus for 100% of sessions with an error
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
});

