output "ip" {
  value = azurerm_linux_virtual_machine.vm.public_ip_address
  sensitive = true
}

output "vault_url" {
  value = azurerm_key_vault.kv.vault_uri
  sensitive = true
}