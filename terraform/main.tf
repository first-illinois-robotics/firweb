locals {
  storage_name = "fir${terraform.workspace}"
  url = "${terraform.workspace}.firstillinoisrobotics.org"
}

resource "azurerm_resource_group" "rg" {
  name     = "fir_tf_${terraform.workspace}"
  location = var.region
}

resource "azurerm_public_ip" "public_ip" {
  name                = "fir_${terraform.workspace}_ip"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_security_group" "nsg" {
  name                = "fir_${terraform.workspace}_nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_network_security_rule" "ssh" {
  name                       = "SSH"
  priority                   = 1001
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "22"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "https" {
  name                       = "HTTPS"
  priority                   = 1003
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "443"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_interface" "nic" {
  name                = "fir_${terraform.workspace}_nic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "my_nic_configuration"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.public_ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "nsg_attach" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_storage_account" "boot_diag_storage" {
  name                     = "fir${terraform.workspace}bootdiag"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                  = "fir_${terraform.workspace}"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  size                  = "Standard_B2pts_v2"

  os_disk {
    name                 = "myOsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-arm64"
    version   = "latest"
  }

  computer_name  = local.url
  admin_username = var.username

  admin_ssh_key {
    username   = var.username
    public_key = var.ssh_pub_key
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.boot_diag_storage.primary_blob_endpoint
  }

  identity {
    type = "SystemAssigned"
  }
}

resource "cloudflare_record" "media" {
  zone_id = var.zone_id
  name    = "${terraform.workspace}media"
  value   = "${local.storage_name}.blob.core.windows.net"
  type    = "CNAME"
  comment = "Auto managed by Terraform"
  proxied = true
}

resource "cloudflare_record" "media_verify" {
  zone_id = var.zone_id
  name    = "asverify.${terraform.workspace}media"
  value   = "asverify.${local.storage_name}.blob.core.windows.net"
  type    = "CNAME"
  comment = "Auto managed by Terraform"
  proxied = false
}

resource "azurerm_storage_account" "web_storage" {
  name                     = local.storage_name
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
  custom_domain {
    name          = "${terraform.workspace}media.firstillinoisrobotics.org"
    use_subdomain = true
  }
  blob_properties {
    cors_rule {
      allowed_headers    = ["Accept-Encoding"]
      allowed_methods    = ["GET", "OPTIONS"]
      allowed_origins    = ["https://firstillinoisrobotics.org","https://*.firstillinoisrobotics.org"]
      exposed_headers    = ["Cache-Control", "Content-Encoding", "Content-Type"]
      max_age_in_seconds = 200
    }
  }
  depends_on = [
    cloudflare_record.media,
    cloudflare_record.media_verify
  ]
}


resource "azurerm_storage_container" "static" {
  name                  = "static"
  storage_account_name  = azurerm_storage_account.web_storage.name
  container_access_type = "blob"
}

resource "azurerm_storage_container" "media" {
  name                  = "media"
  storage_account_name  = azurerm_storage_account.web_storage.name
  container_access_type = "blob"
}

resource "cloudflare_record" "www" {
  zone_id = var.zone_id
  name    = terraform.workspace
  value   = azurerm_linux_virtual_machine.vm.public_ip_address
  type    = "A"
  proxied = true
  comment = "Auto managed by Terraform"
}

resource "tls_private_key" "origin_ca_priv" {
  algorithm = "RSA"
}

resource "tls_cert_request" "origin_ca_request" {
  private_key_pem = tls_private_key.origin_ca_priv.private_key_pem

  subject {
    common_name  = local.url
    organization = "FIRST Illinois Robotics"
  }
}

resource "cloudflare_origin_ca_certificate" "cert" {
  csr                = tls_cert_request.origin_ca_request.cert_request_pem
  hostnames          = [local.url]
  request_type       = "origin-rsa"
  requested_validity = 7
}