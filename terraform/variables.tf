variable "static_rg" {
  type = string
  description = "The name of the Resource Group that contains the static resources to be referenced "
  default = "static_resources"
}

variable "region" {
  type = string
  default = "northcentralus"
}

variable "subnet_id" {
  type = string
  description = "The ID of the Azure Subnet to attach the VM to"
}

variable "zone_id" {
  type = string
  description = "The ID of the Cloudflare Zone to attach the DNS record to"
}

variable "username" {
  type = string
  default = "azureuser"
}

variable "ssh_pub_key" {
  type = string
}