
data "azurerm_client_config" "current" {}


resource "azurerm_key_vault" "kv" {
  name                        = "fir-${terraform.workspace}-kv"
  location                    = azurerm_resource_group.rg.location
  resource_group_name         = azurerm_resource_group.rg.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    secret_permissions = [
      "Get",
      "List",
      "Set",
      "Delete",
    ]
  }
}

resource "random_password" "secret-key" {
  length           = 64
}

resource "azurerm_key_vault_secret" "secret-key" {
  name         = "secret-key"
  value        = random_password.secret-key.result
  key_vault_id = azurerm_key_vault.kv.id
}

resource "random_password" "db-pass" {
  length  = 32
  special = false
}

resource "azurerm_key_vault_secret" "db-pass" {
  name         = "db-pass"
  value        = random_password.db-pass.result
  key_vault_id = azurerm_key_vault.kv.id
}

resource "azurerm_key_vault_secret" "storage-name" {
  name         = "storage-name"
  value        = azurerm_storage_account.web_storage.name
  key_vault_id = azurerm_key_vault.kv.id
}
resource "azurerm_key_vault_secret" "storage-key" {
  name         = "storage-key"
  value        = azurerm_storage_account.web_storage.primary_access_key
  key_vault_id = azurerm_key_vault.kv.id
}
resource "azurerm_key_vault_secret" "origin-ca-key" {
  name         = "origin-ca-key"
  value        = tls_private_key.origin_ca_priv.private_key_pem
  key_vault_id = azurerm_key_vault.kv.id
}
resource "azurerm_key_vault_secret" "origin-ca-cert" {
  name         = "origin-ca-cert"
  value        = cloudflare_origin_ca_certificate.cert.certificate
  key_vault_id = azurerm_key_vault.kv.id
}
