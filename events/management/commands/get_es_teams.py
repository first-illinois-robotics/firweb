from django.core.management import BaseCommand
from sentry_sdk.crons import monitor

from ...scrapers import teams


class Command(BaseCommand):
    help = "Get all active season teams from Elasticsearch"

    def add_arguments(self, parser):
        parser.add_argument(
            "--cron",
            action="store_true",
            help="Run with sentry cron integration",)

    def handle(self, *args, **options):
        if options['cron']:
            with monitor(monitor_slug="get_es_teams"):
                print("Monitoring with cron")
                teams.get_es_teams()
        else:
            teams.get_es_teams()
