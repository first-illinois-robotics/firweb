

from django.db import models


class Competition(models.TextChoices):
    UNKNOWN = "UK", "Unknown"
    FRC = "FRC", "FIRST Robotics Competition"
    FTC = "FTC", "FIRST Tech Challenge"
    FLLC = "FLLC", "FIRST LEGO League Challenge"
    FLLE = "FLLE", "FIRST LEGO League Explore"
    FLLD = "FLLD", "FIRST LEGO League Discover"


class CompetitionNotFound(Exception):
    pass


def clean_competition(comp: str) -> str:
    if comp in Competition.values:
        return comp
    if comp in ['FLL', 'FLLC']:
        return "FLLC"
    if comp in ['JFLL']:
        return "FLLE"
    raise CompetitionNotFound
