from django.http import HttpRequest
from django.shortcuts import render

from events.competitions import Competition
from events.models import Season, TeamYear


def team_list_view(request: HttpRequest, program: Competition):

    all_seasons = Season.objects.filter(competition=program).order_by('-year')

    passed_season = request.GET.get('season')
    if passed_season is None:
        season = all_seasons.latest()
    else:
        season = Season.objects.get(competition=program, year=passed_season)

    teams = TeamYear.objects.filter(season=season).order_by('team__team_num')
    return render(request, 'team_list.html', {'current_season': season, 'all_seasons': all_seasons, 'teams': teams})
