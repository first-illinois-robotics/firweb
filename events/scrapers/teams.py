import logging
import urllib.parse

import requests
from django.db import transaction
from django.http import HttpResponse

from . import ES_BASE_URL
from ..models import Season, Team, TeamYear


@transaction.atomic
def get_es_teams():
    seasons = Season.objects.filter(active=True)
    for season in seasons:
        logging.info(f"Scraping teams for season {season.competition} {season.year}")
        scrape = scrape_season_es_teams(season)
        if isinstance(scrape, HttpResponse):
            return scrape
    return HttpResponse('ok')


@transaction.atomic
def get_all_es_teams():
    seasons = Season.objects.all()
    for season in seasons:
        logging.info(f"Scraping teams for season {season.competition} {season.year}")
        scrape = scrape_season_es_teams(season)
        if isinstance(scrape, HttpResponse):
            return scrape
    return HttpResponse('ok')


def scrape_season_es_teams(season: Season):
    teams: dict = requests.request("GET", f"{ES_BASE_URL}teams/_search", params={
        'q': f"team_type:{season.competition} AND profile_year:{season.year} AND team_stateprov:IL AND team_country:USA",
        'size': '1000',
    }).json()
    if teams.get("error"):
        logging.error(teams)
        return HttpResponse('error', status=500)
    for team_src in teams['hits']['hits']:
        try:
            team_src = team_src['_source']
            try:
                t = TeamYear.objects.get(es_id=team_src['id'])
                # we already have a teamyear object matching this ES id, let's just update it

                if t.team.team_num != team_src["team_number_yearly"]:
                    # team number no longer temporary, update it
                    t.team.team_num = team_src["team_number_yearly"];
                    t.team.save()

                update_teamyear_entry_from_es(team_src, t)
            except (TeamYear.DoesNotExist, KeyError):
                # no TeamYear entry found for this ES instance, let's try seeing if there's one without the ES ID
                try:
                    t = TeamYear.objects.get(season=season, team__team_num=team_src["team_number_yearly"])
                    # teamyear entry exists, just doesn't have the ES id associated. update it anyway
                    update_teamyear_entry_from_es(team_src, t)
                except TeamYear.DoesNotExist:
                    # no entry at all, completely new team, let's go ahead and create it

                    try:
                        team_global_obj = Team.objects.get(competition=season.competition, team_num=team_src["team_number_yearly"])
                    except Team.DoesNotExist:
                        # no global team object found, let's create it
                        team_global_obj = Team(
                            competition=season.competition,
                            team_num=team_src["team_number_yearly"],
                        )
                        team_global_obj.save()
                    team_obj = TeamYear(
                        team=team_global_obj,
                        season=season,
                    )
                    update_teamyear_entry_from_es(team_src, team_obj)
        except KeyError as e:
            logging.warning(f"Failed to parse team {team_src}. Error: {e}")


def update_teamyear_entry_from_es(team_src, team_obj: TeamYear):
    try:
        team_obj.nickname = team_src["team_nickname"]
    except KeyError:
        team_obj.nickname = f"Team {team_src['team_number_yearly']}"
    try:
        team_obj.fullname = team_src["team_name_calc"]
    except KeyError:
        team_obj.fullname = ""
    raw_web = team_src.get("team_web_url", None)
    if raw_web:
        team_obj.website = urllib.parse.urlunparse(urllib.parse.urlparse(raw_web, scheme="http"))

    try:
        team_obj.city = team_src["team_city"]
    except KeyError:
        team_obj.city = "None"
    team_obj.state = team_src["team_stateprov"]
    team_obj.country = team_src["team_country"]

    team_obj.lat = team_src["location"][0]["lat"]
    team_obj.long = team_src["location"][0]["lon"]

    team_obj.es_id = team_src["id"]

    team_obj.save()
