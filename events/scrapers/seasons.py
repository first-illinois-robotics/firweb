import sentry_sdk
import logging
import urllib.parse
from datetime import date

import requests
from django.db import transaction
from django.http import HttpRequest, HttpResponse

from ..competitions import clean_competition, CompetitionNotFound
from . import ES_BASE_URL
from ..models import Season, Team, TeamYear

def get_es_seasons():
    seasons = requests.request("GET", f"{ES_BASE_URL}seasons/_search", params={
        'size': '1000',
        'sort': 'season_year_start:desc'
    }).json()
    if seasons.get("error"):
        logging.error(seasons)
        return HttpResponse('error', status=500)
    for season in seasons['hits']['hits']:
        logging.debug(f"Updating {season}")

        try:
            competition = clean_competition(season['_source']['program_code'])
        except CompetitionNotFound:
            logging.warning(
                f"Found unknown competition: {season['_source']['program_code']}, discarding season silently")
            continue

        try:
            s = Season.objects.get(year=season['_source']['season_year_start'],
                                   competition=competition)
            if s.name != season['_source']['season_name']:
                s.name = season['_source']['season_name']
                s.save()
        except Season.DoesNotExist:
            if date.today().year - 1 < season['_source']['season_year_start']:
                active = True
            else:
                active = False
            s = Season(year=season['_source']['season_year_start'], competition=competition,
                       name=season['_source']['season_name'], active=active)
            s.save()
            logging.debug(f"Created {s}")